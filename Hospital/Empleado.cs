﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Empleado: Persona
    {
        
        // Atributos
        
        private String especialidad;
        private Double sueldo; // 500 , 505.3546
        private int mesesTrabajados; // 500, 600 


        // Metodos

        public double calcularSueldoPorMesesTrabajados()
        {
            // Valida datos
            if(mesesTrabajados < 1)
            {
                return 0;
            }

            if(sueldo < 0)
            {
                return 0;
            }
            return mesesTrabajados * sueldo;
        }

        // 0
        public double calcularSueldoPorMesesTrabajados(double porcentajeDescuento)
        {
            return calcularSueldoPorMesesTrabajados()  * porcentajeDescuento; 
        }


        public virtual  String hola() {
            return "HOLA";
        }

        // 
        public override string ToString()
        {
            return Nombre + " " + Apellido;
        }

        // Gets ands Sets

        public Double Sueldo
        {
            get { return sueldo; }
            set { sueldo = value; }
        }

        public String Especialidad
        {
            get { return especialidad; }
            set { especialidad = value; }
        }


        public int MesesTrabajados
        {
            get { return mesesTrabajados; }
            set { mesesTrabajados = value; }
        }



    }
}
